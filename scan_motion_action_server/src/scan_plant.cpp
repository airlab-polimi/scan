#include "ros/ros.h"
#include <math.h>
#include "scan_motion/scan_plant.h"
#include "grape_msgs/ScanPlantAction.h"
#include <actionlib/server/simple_action_server.h>
#include "vs_grasping/GetJointsSafeForWires.h"



#define NODE_NAME "scan_plant_server"
#define NODE_RATE 100

scan_plant::scan_plant() : 
                            Handle(),
                            group("arm"),
                            jointPositions_untangled(6),
                            jaco2_model_loader("robot_description"),
                            scanServer(Handle, "scan_plant", boost::bind(&scan_plant::newScanReceived, this, _1), false) {
    // Initialize MoveIt! for joint control
    jaco2_kinematic_model = jaco2_model_loader.getModel();
    jaco2_kinematic_state = robot_state::RobotStatePtr(new robot_state::RobotState(jaco2_kinematic_model));
    jaco2_joint_model_group = jaco2_kinematic_model->getJointModelGroup("arm");
    isJacobianValid   = false;
    activateMovement  = false;
    errorInMotion     = false;
    rotateEndEffector = false;
}

void scan_plant::getYamlParams(){
    if (!Handle.getParam("/scan_plant_server/initial_scan_position", initialScanPosition))
        ROS_ERROR("Node %s: unable to retrieve parameter initial_scan_position.", ros::this_node::getName().c_str());
    if (!Handle.getParam("/scan_plant_server/navigation_pose", navigationPose))
        ROS_ERROR("Node %s: unable to retrieve parameter navigation_pose.", ros::this_node::getName().c_str());
    if (!Handle.getParam("/scan_plant_server/path_length", pathLength))
        ROS_ERROR("Node %s: unable to retrieve parameter path_length.", ros::this_node::getName().c_str());
    std::vector<double> speedVector_std;
    if (!Handle.getParam("/scan_plant_server/speed_vector", speedVector_std))
        ROS_ERROR("Node %s: unable to retrieve parameter speed_vector.", ros::this_node::getName().c_str());
    speedVector.x = speedVector_std.at(0);
    speedVector.y = speedVector_std.at(1);
    speedVector.z = speedVector_std.at(2);    
    // tolerance values, with default
    Handle.param<double>("/scan_plant_server/distance_from_target_tolerance", distanceFromTargetTolerance, 0.05);
    Handle.param<double>("/scan_plant_server/distance_from_start_threshold", distanceFromStartThreshold, 0.05);
    

}

void scan_plant::prepare(){
    getYamlParams();
    getJointsSafeForWires_cli  = Handle.serviceClient<vs_grasping::GetJointsSafeForWires>("get_joints_safe_for_wires");
    _jointVelocity_publisher   = Handle.advertise<kinova_msgs::JointVelocity>("/j2n6s300_driver/in/joint_velocity", 1);
    _jointPositions_subscriber = Handle.subscribe("j2n6s300_driver/out/joint_angles", 1, &scan_plant::jointPositions_MessageCallback, this);
    _toolPose_subscriber       = Handle.subscribe("j2n6s300_driver/out/tool_pose", 1, &scan_plant::toolPose_MessageCallback, this); 
    testTopic = Handle.subscribe("test_topic", 1, &scan_plant::prova, this); 
    initManipulator();
    scanServer.start();
    ROS_INFO("Action server scan_plant is running");
}

void scan_plant::translatePlan2currentJacoPosition(moveit::planning_interface::MoveGroupInterface::Plan& plan)
{
    // translate all the trajectory points considering the current joint state
    std::vector<double> act_joint_pos = group.getCurrentJointValues();
    for (int i=0; i<plan.trajectory_.joint_trajectory.points.size(); i++) {
        for (int j=0; j<act_joint_pos.size(); j++) {
            plan.trajectory_.joint_trajectory.points.at(i).positions.at(j) += act_joint_pos.at(j)-plan.start_state_.joint_state.position.at(j);
        }
    }

    // translate the initial state
    for (int i=0; i<act_joint_pos.size(); i++) {
        plan.start_state_.joint_state.position.at(i) = act_joint_pos.at(i);
    }   
}

bool scan_plant::moveArm(std::vector<double> targetArmPose){
    group.setStartStateToCurrentState();
    group.setJointValueTarget(targetArmPose);
    if(group.plan(plan)){
        translatePlan2currentJacoPosition(plan);
        errorCode = group.execute(plan);
        // rotateEndEffectorBack();
    }
    else {
        ROS_ERROR("Cannot plan trajectory");
        errorCode = 0;
    }
}

geometry_msgs::Pose scan_plant::calculateFinalPoint(){

    geometry_msgs::Vector3 directionVector;
    double speedVector_module = sqrt(speedVector.x*speedVector.x + speedVector.y*speedVector.y + speedVector.z*speedVector.z);
    directionVector.x = speedVector.x/speedVector_module;
    directionVector.y = speedVector.y/speedVector_module;
    directionVector.z = speedVector.z/speedVector_module;

    //geometry_msgs::Pose currentPose = group.getCurrentPose().pose;
    finalPose.position.x = currentPose.position.x + directionVector.x * pathLength;
    finalPose.position.y = currentPose.position.y + directionVector.y * pathLength;
    finalPose.position.z = currentPose.position.z + directionVector.z * pathLength;
}

void scan_plant::untangle_joint345(){
    std::vector<double> targetPosition(3);
    std::vector<double> referencePosition(3);
    std::vector<double> currentPosition(3);
    std::vector<double> distanceFromTarget(3);
    std::vector<double> needsRotation(3);
    std::vector<int> sign(3);
    bool someoneNeedsRotation = false;

    for(int i = 0; i < 3; i++){
        currentPosition.at(i) = joint_values.at(3+i);
        referencePosition.at(i) = jointPositions_untangled.at(3+i);
        targetPosition.at(i) = currentPosition.at(i) + 2*M_PI*std::min(std::ceil((referencePosition.at(i) - currentPosition.at(i) - M_PI)/(2*M_PI)), 
                                                                    std::floor((referencePosition.at(i) - currentPosition.at(i) + M_PI)/(2*M_PI)));
        distanceFromTarget.at(i) = targetPosition.at(i) - currentPosition.at(i);
        sign.at(i) = (targetPosition.at(i) - currentPosition.at(i) > 0) ? 1 : -1;
        if(std::fabs(distanceFromTarget.at(i)) > M_PI){
            someoneNeedsRotation = true;
            needsRotation.at(i) = 1;
        } else {
            needsRotation.at(i) = 0;
        } 
    }


    if(!someoneNeedsRotation){
        ROS_INFO("No need to untangle");
        return;
    }
    
    kinova_msgs::JointVelocity joint0_velocity;
    joint0_velocity.joint1 = 0;
    joint0_velocity.joint2 = 0;
    joint0_velocity.joint3 = 0;
    joint0_velocity.joint4 = needsRotation.at(0)*sign.at(0)*30;
    joint0_velocity.joint5 = needsRotation.at(1)*sign.at(1)*30;
    joint0_velocity.joint6 = needsRotation.at(2)*sign.at(2)*30;

    ROS_INFO("velocità: %f %f %f", joint0_velocity.joint4);
    ros::Rate r(NODE_RATE);
    while (needsRotation.at(0) != 0 || needsRotation.at(1) != 0 || needsRotation.at(2) != 0)
    {
        ros::spinOnce();

        for(int i = 0; i < 3; i++) {
            distanceFromTarget.at(i) = joint_values.at(3+i) - targetPosition.at(i);
            if(std::fabs(distanceFromTarget.at(i)) < distanceFromTargetTolerance)
                needsRotation.at(i) = 0;
        }

        _jointVelocity_publisher.publish(joint0_velocity);
        r.sleep();
    }

}


void scan_plant::newScanReceived(const grape_msgs::ScanPlantGoalConstPtr &goal){
    // retrieve reference values to avoid wires untanglement
    vs_grasping::GetJointsSafeForWires srv;
    getJointsSafeForWires_cli.call(srv);
    jointPositions_untangled.at(0) = srv.response.joint0;
    jointPositions_untangled.at(1) = srv.response.joint1;
    jointPositions_untangled.at(2) = srv.response.joint2;
    jointPositions_untangled.at(3) = srv.response.joint3;
    jointPositions_untangled.at(4) = srv.response.joint4;
    jointPositions_untangled.at(5) = srv.response.joint5;

    // in order to be sure that the laser is in correct position during the whole movement, 
    // the first pose (initialScanPosition) is given in joint space to have full control of the position,
    // while the final position is given as a goal point (x,y,z) that is reached mantaining the end effector orientation
    grape_msgs::ScanPlantResult scanResult;
    grape_msgs::ScanPlantFeedback scanFeedback;

    previousDistance = std::numeric_limits<double>::infinity();
    ROS_INFO("ScanServer: new goal received");
    moveArm(initialScanPosition);
    // wait half a sec, to be sure that toolPose_MessageCallback has been called at least once since the arm in in the desired pose
    ros::Duration(0.5).sleep();
    actualInitialScanPosition = currentPose;
    if(!errorCode){
        scanResult.actual_motion_duration = ros::Duration(-1);
        scanServer.setAborted(scanResult);
        ROS_ERROR("aborting scanServer motion (A)");
    }
    else{
        untangle_joint345();
		calculateFinalPoint();
		errorInMotion = false;
		activateMovement = true;
		ros::Time startMovement = ros::Time::now();
		while(activateMovement){
			ros::Duration(0.1).sleep();
		}
		ros::Time endMovement = ros::Time::now();
		if(errorInMotion) {
			scanResult.actual_motion_duration = ros::Duration(-1);
			scanServer.setAborted(scanResult);
			ROS_ERROR("aborting scanServer motion (B)");
		} else {
			scanResult.actual_motion_duration = endMovement - startMovement;
			scanServer.setSucceeded(scanResult);
		}
	}

}

void scan_plant::initManipulator(){

    group.setPlannerId("PRMstarkConfigDefault");
    group.setPlanningTime(8.0);
    group.setGoalPositionTolerance(0.0001);
    group.setGoalOrientationTolerance(0.001);
    group.setGoalJointTolerance(0.0001);
    // group.setPoseReferenceFrame("j2n6s300_link_base");
}

void scan_plant::toolPose_MessageCallback(const geometry_msgs::PoseStamped::ConstPtr& pose)
{
    currentPose = pose->pose;
    if(!activateMovement)
        return;
    double distanceFromTarget = sqrt(pow((currentPose.position.x - finalPose.position.x),2) +
                                     pow((currentPose.position.y - finalPose.position.y),2) +
                                     pow((currentPose.position.z - finalPose.position.z),2)); 
    double distanceFromInitialPose = sqrt(pow((currentPose.position.x - actualInitialScanPosition.position.x),2) +
                                          pow((currentPose.position.y - actualInitialScanPosition.position.y),2) +
                                          pow((currentPose.position.z - actualInitialScanPosition.position.z),2)); 
                                          
    ROS_DEBUG("distanceFromTarget: %f, (%f %f) (%f %f) (%f %f)",distanceFromTarget, currentPose.position.x, finalPose.position.x,
                                                                                    currentPose.position.y, finalPose.position.y,
                                                                                    currentPose.position.z, finalPose.position.z);
    
    if(distanceFromTarget <= distanceFromTargetTolerance){
        ROS_INFO("Goal position reached");
        activateMovement = false;
    }
    // it means that an error occurred during the trajectory execution, very likely because of a singularity
    // the movement must be stopped, because we're out of control! 
    else if (distanceFromTarget > previousDistance && 
             distanceFromInitialPose > distanceFromStartThreshold) {
        activateMovement = false;
        errorInMotion = true;
    }
    previousDistance = distanceFromTarget;
}


void scan_plant::jointPositions_MessageCallback(const kinova_msgs::JointAngles::ConstPtr& jointPositions)
{
    joint_values = {jointPositions->joint1*M_PI/180.0,
                    jointPositions->joint2*M_PI/180.0,
                    jointPositions->joint3*M_PI/180.0,
                    jointPositions->joint4*M_PI/180.0,
                    jointPositions->joint5*M_PI/180.0,
                    jointPositions->joint6*M_PI/180.0};
    
    // check if I have to stop rotate the end effector
    if(rotateEndEffector){
        if(fabs(joint_values.at(5) - targetJointRotation) < distanceFromTargetTolerance) {
            rotateEndEffector = false;
        }
    }

    // if I'm moving, check if I'm arrived
    if(!activateMovement)
        return;
    // else, calculate velocities
    jaco2_kinematic_state->setJointGroupPositions(jaco2_joint_model_group, joint_values);

    _jacobian = jaco2_kinematic_state->getJacobian(jaco2_joint_model_group);
    isJacobianValid = true;
}


void scan_plant::publishJointSpeeds(){
    if(!isJacobianValid || !activateMovement)
        return;
    Eigen::VectorXd cartesian_velocity(6);
    cartesian_velocity(0) = speedVector.x;
    cartesian_velocity(1) = speedVector.y;
    cartesian_velocity(2) = speedVector.z;
    cartesian_velocity(3) = 0.0;
    cartesian_velocity(4) = 0.0;
    cartesian_velocity(5) = 0.0;

    Eigen::VectorXd _jointVelocity = _jacobian.fullPivLu().solve(cartesian_velocity.cast<double>())*180/M_PI;

    /* Send velocity command to the robot */
    kinova_msgs::JointVelocity joint_velocity_cmd;

    joint_velocity_cmd.joint1 = _jointVelocity(0);
    joint_velocity_cmd.joint2 = _jointVelocity(1);
    joint_velocity_cmd.joint3 = _jointVelocity(2);
    joint_velocity_cmd.joint4 = _jointVelocity(3);
    joint_velocity_cmd.joint5 = _jointVelocity(4);
    joint_velocity_cmd.joint6 = _jointVelocity(5);

    _jointVelocity_publisher.publish(joint_velocity_cmd);
}


void scan_plant::prova(const std_msgs::Empty::ConstPtr& dummy){
    rotateEndEffectorBack();
    return;
}

// rotates the last joint until the rotation angle is in [0;2pi]. 
// use this to disentangle the wires
void scan_plant::rotateEndEffectorBack(){
    // until it's not ok
    return;

    this->initialJointRotation = joint_values.at(5);
    int entireRotations = initialJointRotation/(2*M_PI);
    // this is for going to [0;2pi]
    // remove this step for going to [-2pi;2pi]
    if(joint_values.at(5) < 0)
        entireRotations = entireRotations - 1;
    targetJointRotation = initialJointRotation - entireRotations*2*M_PI;
    int sign = initialJointRotation > 0 ? -1 : 1;
    end_effector_velocity_cmd.joint1 = 0;
    end_effector_velocity_cmd.joint2 = 0;
    end_effector_velocity_cmd.joint3 = 0;
    end_effector_velocity_cmd.joint4 = 0;
    end_effector_velocity_cmd.joint5 = 0;
    end_effector_velocity_cmd.joint6 = sign*30;
    // activate rotation of the end effector 
    if(entireRotations)
        rotateEndEffector = true;  

    ros::Rate r(NODE_RATE);
    while (rotateEndEffector)
    {
        ros::spinOnce();
        _jointVelocity_publisher.publish(end_effector_velocity_cmd);
        r.sleep();
    }
    ROS_INFO("End effector is in [0;2pi]"); 
}


void scan_plant::runPeriodically(){
    ros::Rate LoopRate(NODE_RATE);
    ROS_INFO("Node %s running periodically (T=%.2fs, f=%dHz).", ros::this_node::getName().c_str(), 1.0/NODE_RATE, NODE_RATE);

    while (ros::ok()){
        ros::spinOnce();
        publishJointSpeeds();
        LoopRate.sleep();
    }
}

/*****************************************************************/


int main(int argc, char** argv){
  ros::init(argc, argv, NODE_NAME);
  scan_plant mNode;
  mNode.prepare();
  mNode.runPeriodically();

  return 0;
}
