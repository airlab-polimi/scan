#include "ros/ros.h"
#include <actionlib/server/simple_action_server.h>
#include "kinova_msgs/JointVelocity.h"
#include "kinova_msgs/JointAngles.h"
// moveit
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit/robot_model_loader/robot_model_loader.h>

// grape_msgs
#include "grape_msgs/ScanPlantAction.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Empty.h"
// service
#include "vs_grasping/GetJointsSafeForWires.h"



typedef actionlib::SimpleActionServer<grape_msgs::ScanPlantAction> ScanServer;

class scan_plant {
public:
	scan_plant();
	void prepare();
	void runPeriodically();
private:
	ros::NodeHandle Handle;
	void getYamlParams();

	// wires handling
	std::vector<double> jointPositions_untangled;
	void untangle_joint345();
	ros::ServiceClient getJointsSafeForWires_cli;

	// publishers and subscribers, and callbacks
	ros::Subscriber _jointPositions_subscriber;
	ros::Subscriber _toolPose_subscriber;
	ros::Publisher  _jointVelocity_publisher;
	void jointPositions_MessageCallback(const kinova_msgs::JointAngles::ConstPtr& jointPositions);
	void toolPose_MessageCallback(const geometry_msgs::PoseStamped::ConstPtr& pose);
	
	// joint position handling
	Eigen::MatrixXd _jacobian;
	void publishJointSpeeds();
	void translatePlan2currentJacoPosition(moveit::planning_interface::MoveGroupInterface::Plan& plan);
	geometry_msgs::Pose finalPose;
	bool isJacobianValid;
	bool activateMovement;
	
	// Action server
	ScanServer scanServer;
	grape_msgs::ScanPlantFeedback scanFeedback;
	void newScanReceived(const grape_msgs::ScanPlantGoalConstPtr &goal);
	
	// Moveit!
	void initManipulator();
	bool moveArm(std::vector<double> targetArmPose);
	moveit::planning_interface::MoveGroupInterface::Plan plan;
	moveit::planning_interface::MoveGroupInterface group;
	robot_model_loader::RobotModelLoader jaco2_model_loader;
	robot_model::RobotModelPtr jaco2_kinematic_model;
	robot_state::RobotStatePtr jaco2_kinematic_state;
	robot_state::JointModelGroup* jaco2_joint_model_group;
	
	// variables for the handling of the poses
	geometry_msgs::Pose currentPose;
	std::vector<double> navigationPose;
	// goal scan initial position
	std::vector<double> initialScanPosition;
	// actual scan initial pose, used to avoid false positives in error detection
	geometry_msgs::Pose actualInitialScanPosition;
	geometry_msgs::Vector3 offsetVector;
	geometry_msgs::Vector3 speedVector;
 	double pathLength;
 	double distanceFromTargetTolerance;
 	double distanceFromStartThreshold;
 	// this values are compared with the current distance at each iteration, in order to detect errors 
 	double previousDistance;
 	bool errorInMotion;

 	// strcutures and methods for the rotation of the end effector
 	// to untangle wires of sensors on end effector
 	bool rotateEndEffector;
 	double targetJointRotation;
 	double initialJointRotation;
 	std::vector<double> joint_values;
 	// dummy function for testing the untanglement procedure
 	void prova(const std_msgs::Empty::ConstPtr& dummy);
 	ros::Subscriber testTopic;
 	void rotateEndEffectorBack();
    kinova_msgs::JointVelocity end_effector_velocity_cmd;



 	// helper
 	geometry_msgs::Pose calculateFinalPoint();
	// variables for filling the action result
	ros::Duration motionDuration;
	moveit::planning_interface::MoveItErrorCode errorCode;	
};

