# README
---
ONLY FOR POLIMI PEOPLE: if you need to use this repo, please clone also **common_pkgs** (https://bitbucket.org/airlab-polimi/common_pkgs) repo in the same workspace. It contains packages that are shared with other repositories of the project.